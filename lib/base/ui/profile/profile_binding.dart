

import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:github/base/repository/model/github_user_entity.dart';
import 'package:github/base/repository/model/user_info_entity.dart';
import 'package:github/base/shared/constant/common.dart';

import '../../base_controller.dart';


class ProfileBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ProfileController());
  }
}

class ProfileController extends BaseController {
late InfoUser userInfo ;
dynamic argumemtData= Get.arguments;
  @override
  void onInit() async {
    onGetUser(argumemtData.toString());
    super.onInit();
  }

  Future<void> onGetUser(String id) async {
    setScreenState = screenStateLoading;
    final res = await getUser(id);
    String text = '';
    if (res.status == true) {
      try {
        logWhenDebug("x===", res.body.toString());
        setScreenState = screenStateOk;
        userInfo = InfoUser.fromJson(res.body);
      } catch (e) {
        text = e.toString();
      }
    } else {
      text = res.text ?? Common().string.error_message;
      setScreenState = screenStateError;
    }
    if (res.isError == true) {
      text = Common().string.error_message;
      setScreenState = screenStateError;
    }
    if (text.isNotEmpty) {
      showSnackBar(title: "Response", message: text);
    }
    update();
  }

}
