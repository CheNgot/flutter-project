import 'package:flutter/material.dart';
import 'package:github/base/shared/constant/common.dart';
import 'package:github/base/ui/profile/profile_binding.dart';

import '../../base_view_view_model.dart';

class ProfileScreen extends BaseView<ProfileController> {
  @override
  Widget vBuilder() => Scaffold(
        appBar: AppBar(
          leading: BackButton(color: Colors.black),
          title: Text("User Info",style: TextStyle(color: Colors.black),),
          centerTitle: true,
          backgroundColor: Colors.white,

        ),
        body: _body(),
      );

  _body() {
    if (controller.screenStateIsLoading)
      return Center(child: CircularProgressIndicator());
    if (controller.screenStateIsError)
      return Text(Common().string.error_message);
    if (controller.screenStateIsOK) return buildWidgetListUser();
  }

  buildWidgetListUser() => controller.userInfo.login == null
      ? Container()
      : RefreshIndicator(
          color: Colors.white,
          onRefresh: () async {
            controller.onGetUser(Get.arguments.toString());
          },
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                      color: Colors.red,
                      width: MediaQuery.of(Get.context!).size.width,
                      height: 150,
                      child: Image(
                        fit: BoxFit.cover,
                        image: AssetImage("assets/images/back_ground.jpg"),
                      )),
                  Container(
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.black,
                        image: DecorationImage(
                            image: NetworkImage(
                                controller.userInfo.avatarUrl.toString()))),
                    margin: EdgeInsets.all(50),
                    width: MediaQuery.of(Get.context!).size.width,
                    height: 130,
                  )
                ],
              ),
              Text(controller.userInfo.login.toString()),
              Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  children: [
                    Text(
                      "Location",
                      style: TextStyle(fontSize: 25),
                    ),
                    Text(
                      controller.userInfo.location.toString(),
                    ),
                  ],
                ),
              ),
              Card(
                elevation: 8,
                shadowColor: Colors.grey,
                child: Container(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      Expanded(
                          child: Column(
                        children: [
                          Text(
                            controller.userInfo.followers.toString(),
                            style: TextStyle(fontSize: 25),
                          ),
                          Text("Follower"),
                        ],
                      )),
                      Expanded(
                          child: Column(
                        children: [
                          Text(
                            controller.userInfo.following.toString(),
                            style: TextStyle(fontSize: 25),
                          ),
                          Text("Following")
                        ],
                      )),
                      Expanded(
                          child: Column(
                        children: [
                          Text(
                            controller.userInfo.publicRepos.toString(),
                            style: TextStyle(fontSize: 25),
                          ),
                          Text("Repos"),
                        ],
                      ))
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(8),
                alignment: Alignment.centerLeft,
                child: Column(
                  children: [
                    Text(
                      "Bio",
                      style: TextStyle(fontSize: 25),
                    ),
                    Text(
                      controller.userInfo.bio.toString(),
                    ),
                  ],
                ),
              ),
            ],
          ));
}
