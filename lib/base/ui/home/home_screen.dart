import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:github/base/routes/app_pages.dart';
import 'package:github/base/shared/constant/common.dart';


import '../../base_view_view_model.dart';
import 'home_binding.dart';

/*
Created by ToanDev on 02/05/2021
Company: Netacom.
Email: hvtoan.dev@gmail.com
*/

class HomeScreen extends BaseView<HomeController> {
  @override
  Widget vBuilder() => Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            "User List",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
        ),
        body: _body(),
      );

  _body() {
    print(
        "filteredMarketData==" + (controller.listData?.length ?? 0).toString());
    if (controller.screenStateIsLoading)
      return Center(child: CircularProgressIndicator());
    if (controller.screenStateIsError)
      return Text(Common().string.error_message);
    if (controller.screenStateIsOK) return buildWidgetListUser();
  }

  buildWidgetListUser() => controller.listData?.length == 0
      ? Container()
      : RefreshIndicator(
          onRefresh: () async {
            controller.onRefresh();
          },
          child: ListView.builder(
          itemCount: controller.listData?.length,
          itemBuilder: (BuildContext context, int index) {
            final item = controller.listData![index];
            return InkWell(
              onTap: () async {
                Get.toNamed(Routes.INFOR,arguments:item.id);
                print("test123=="+item.id.toString());
          },
              child: Container(
                padding: EdgeInsets.all(8),
                child: Row(
                  children: [
                    CachedNetworkImage(
                      imageBuilder: (context, imageProvider) => Container(
                        padding: EdgeInsets.all(5),
                        width: 80.0,
                        height: 80.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: imageProvider, fit: BoxFit.cover),
                        ),
                      ),
                      imageUrl: item.avatarUrl.toString(),
                      placeholder: (context, url) => CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(item.login.toString()),
                            Text(item.url.toString()),
                          ],
                        ))
                  ],
                ),
              ),
            );
          },
        ));
}
