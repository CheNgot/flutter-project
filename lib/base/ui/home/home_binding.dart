import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:github/base/repository/model/github_user_entity.dart';
import 'package:github/base/services/storage_service.dart';
import 'package:github/base/shared/constant/common.dart';

import '../../base_controller.dart';



/*
Created by ToanDev on 05/05/2021
Company: Netacom.
Email: hvtoan.dev@gmail.com
*/
class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeController());
  }
}

class HomeController extends BaseController {
  List<GithubUserEntity>? listData;
  StorageService saveListOffline = StorageService();

  @override
  void onInit() async {
    super.onInit();
    onGetListUser();
    logWhenDebug("CURRENT listuser: ", listData.toString());
  }

  Future<void> onGetListUser() async {
    if(await hasNetwork()){
      setScreenState = screenStateLoading;
      final res = await getListUser();
      String text = '';
      if (res.status == true) {
        try {
          logWhenDebug("x===", res.body.toString());
          // jsonDecode(res.body).map((x) {
          //   logWhenDebug("x===", x.toString());
          // });
          listData = List<GithubUserEntity>.from(res.body.map((x) => GithubUserEntity.fromJson(x)));
          logWhenDebug("listData===", listData.toString());
          GetStorage().writeIfNull("listUser", listData);

          setScreenState = screenStateOk;
        } catch (e) {
          text = e.toString();
        }
      } else {
        text = res.text ?? Common().string.error_message;
        setScreenState = screenStateError;
      }
      if (res.isError == true) {
        text = Common().string.error_message;
        setScreenState = screenStateError;
      }
      if (text.isNotEmpty) {
        showSnackBar(title: "Response", message: text);
      }
      update();
    }
    else{
        if(GetStorage().hasData("listUser"))
          {
            listData= List<GithubUserEntity>.from(GetStorage().read("listUser").map((x) => GithubUserEntity.fromJson(x)));
            setScreenState=screenStateOk;
          }


    }

  }
  void onRefresh(){
    onGetListUser();
  }
  Future<bool> hasNetwork() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      return result.isNotEmpty && result[0].rawAddress.isNotEmpty;
    } on SocketException catch (_) {
      return false;
    }
  }
}