import 'package:get/get.dart';
import 'package:github/base/ui/home/home_binding.dart';
import 'package:github/base/ui/home/home_screen.dart';
import 'package:github/base/ui/profile/profile_binding.dart';
import 'package:github/base/ui/profile/profile_screen.dart';



part 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: Routes.HOME,
      page: () => HomeScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.INFOR,
      page: () => ProfileScreen(),
      binding: ProfileBinding(),
    ),

  ];
}
