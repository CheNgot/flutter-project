class GithubUserEntity {
  String? _login;
  int? _id;
  String? _nodeId;
  String? _avatarUrl;
  String? _gravatarId;
  String? _url;
  String? _htmlUrl;
  String? _followersUrl;
  String? _followingUrl;
  String? _gistsUrl;
  String? _starredUrl;
  String? _subscriptionsUrl;
  String? _organizationsUrl;
  String? _reposUrl;
  String? _eventsUrl;
  String? _receivedEventsUrl;
  String? _type;
  bool? _siteAdmin;



  GithubUserEntity.fromJson(Map<String, dynamic> json) {
    _login = json['login'];
    _id = json['id'];
    _nodeId = json['node_id'];
    _avatarUrl = json['avatar_url'];
    _gravatarId = json['gravatar_id'];
    _url = json['url'];
    _htmlUrl = json['html_url'];
    _followersUrl = json['followers_url'];
    _followingUrl = json['following_url'];
    _gistsUrl = json['gists_url'];
    _starredUrl = json['starred_url'];
    _subscriptionsUrl = json['subscriptions_url'];
    _organizationsUrl = json['organizations_url'];
    _reposUrl = json['repos_url'];
    _eventsUrl = json['events_url'];
    _receivedEventsUrl = json['received_events_url'];
    _type = json['type'];
    _siteAdmin = json['site_admin'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['login'] = this._login;
    data['id'] = this._id;
    data['node_id'] = this._nodeId;
    data['avatar_url'] = this._avatarUrl;
    data['gravatar_id'] = this._gravatarId;
    data['url'] = this._url;
    data['html_url'] = this._htmlUrl;
    data['followers_url'] = this._followersUrl;
    data['following_url'] = this._followingUrl;
    data['gists_url'] = this._gistsUrl;
    data['starred_url'] = this._starredUrl;
    data['subscriptions_url'] = this._subscriptionsUrl;
    data['organizations_url'] = this._organizationsUrl;
    data['repos_url'] = this._reposUrl;
    data['events_url'] = this._eventsUrl;
    data['received_events_url'] = this._receivedEventsUrl;
    data['type'] = this._type;
    data['site_admin'] = this._siteAdmin;
    return data;
  }

  GithubUserEntity(
      this._login,
      this._id,
      this._nodeId,
      this._avatarUrl,
      this._gravatarId,
      this._url,
      this._htmlUrl,
      this._followersUrl,
      this._followingUrl,
      this._gistsUrl,
      this._starredUrl,
      this._subscriptionsUrl,
      this._organizationsUrl,
      this._reposUrl,
      this._eventsUrl,
      this._receivedEventsUrl,
      this._type,
      this._siteAdmin);

  String? get login => _login;

  bool? get siteAdmin => _siteAdmin;

  String? get type => _type;

  String? get receivedEventsUrl => _receivedEventsUrl;

  String? get eventsUrl => _eventsUrl;

  String? get reposUrl => _reposUrl;

  String? get organizationsUrl => _organizationsUrl;

  String? get subscriptionsUrl => _subscriptionsUrl;

  String? get starredUrl => _starredUrl;

  String? get gistsUrl => _gistsUrl;

  String? get followingUrl => _followingUrl;

  String? get followersUrl => _followersUrl;

  String? get htmlUrl => _htmlUrl;

  String? get url => _url;

  String? get gravatarId => _gravatarId;

  String? get avatarUrl => _avatarUrl;

  String? get nodeId => _nodeId;

  int? get id => _id;
}
